package com.app.readcsvfile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.app.readcsvfile.adapter.ListofFilesAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.app.readcsvfile.R;


public class MainActivity extends AppCompatActivity {
    private ArrayList<File> listOfFiles;
    RecyclerView rvListofFiles;
    ListofFilesAdapter mAdapter;
    TextView noRecordFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listOfFiles = new ArrayList<>();

        rvListofFiles = (RecyclerView) findViewById(R.id.rvListofFiles);
        noRecordFound = (TextView) findViewById(R.id.text);

        rvListofFiles.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        mAdapter = new ListofFilesAdapter(this, listOfFiles);
        mAdapter.setOnClickListener(new ListofFilesAdapter.OnClickListener() {
            @Override
            public void onclick(File file) {
                Intent intent = new Intent(MainActivity.this, ReadCSVFileActivity.class);
                intent.putExtra(ReadCSVFileActivity.FILE_PATH, file.getAbsolutePath());
                startActivity(intent);
            }
        });
        rvListofFiles.setAdapter(mAdapter);


        getListOfFilesFromIV_Acuity_TEST();
    }

    private void getListOfFilesFromIV_Acuity_TEST() {
        String path = Environment.getExternalStorageDirectory() + File.separator + "IV_Acuity_test";
        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files == null || files.length == 0) {
                noRecordFound.setVisibility(View.VISIBLE);
            } else {
                noRecordFound.setVisibility(View.GONE);
                for (int i = 0; i < files.length; i++) {
                    listOfFiles.add(files[i]);
                }

                mAdapter.setList(listOfFiles);
            }
        } else {
            try {
                directory.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }
            noRecordFound.setVisibility(View.VISIBLE);
        }


    }
}
