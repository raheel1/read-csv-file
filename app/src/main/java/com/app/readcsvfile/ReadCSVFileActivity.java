package com.app.readcsvfile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadCSVFileActivity extends AppCompatActivity {

    public static final String FILE_PATH = "file_path";
    TextView textData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_c_s_v_file);

        textData = findViewById(R.id.textData);

        String filePath = getIntent().getStringExtra(FILE_PATH);

        readFile(filePath);
    }

    public void readFile(String filePath) {
        boolean dataFound = false; // when heading are read, then this boolean is updated to true. now the next line read will be data.
        String displayText = "";
        File file = new File(filePath);
        if (file.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.toLowerCase().trim().startsWith("trialid")) {
                        String[] tokens = line.split(",");
                        for (int i = 0; i < tokens.length; i++) {
                            displayText += tokens[i] + "     ";
                        }


                        dataFound = true;
                    } else {
                        if (dataFound) {
                            if (line.toLowerCase().trim().startsWith("logmar")) {
                                break;
                            }
                            displayText += "\n";
                            String[] tokens = line.split(",");
                            for (int i = 0; i < tokens.length; i++) {
                                displayText += tokens[i] + "               ";
                            }
                        }
                    }


                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            textData.setText(displayText);

        } else {
            textData.setText("Could not open file.");
        }
    }
}
