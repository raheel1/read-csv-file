package com.app.readcsvfile.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.readcsvfile.MainActivity;
import com.app.readcsvfile.R;

import java.io.File;
import java.util.ArrayList;

public class ListofFilesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<File> listofFiles;
    private OnClickListener onClickListener;


    public ListofFilesAdapter(Context context, ArrayList<File> listOfFiles) {
        this.context = context;
        this.listofFiles = listOfFiles;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(context).inflate(R.layout.item_adapter, null));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            ((ItemViewHolder) holder).rlParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener!=null){
                        onClickListener.onclick(listofFiles.get(position));
                    }


                }
            });
            ((ItemViewHolder) holder).txtTitle.setText(listofFiles.get(position).getName());
        }

    }

    @Override
    public int getItemCount() {
        return listofFiles.size();
    }

    public void setList(ArrayList<File> listOfFiles) {
        this.listofFiles = listOfFiles;
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        RelativeLayout rlParent;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            rlParent = (RelativeLayout) itemView.findViewById(R.id.rlParent);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener{
        public  void onclick(File file);
    }
}
